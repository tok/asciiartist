#!/bin/env python3

"""
asciiartist - https://codeberg.org/tok/asciiartist
Copyright (C) 2021 tok

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PIL import Image, ImageFont, ImageDraw, ImageColor

import numpy as np
from numpy.fft import fft2, ifft2, fftshift

from skimage.io import imshow, show, imread
from skimage.filters import gaussian, threshold_otsu

from skimage.transform import resize, rescale
from skimage import color

from skimage.morphology import skeletonize, erosion, square

import sys
import os

from argparse import ArgumentParser


CHARS_PIXELS = ''.join([chr(c) for c in range(0x2580, 0x25A0)]) # Pixels
CHARS_EXTENDED_PIXELS = ''.join([chr(c) for c in range(0x2580, 0x2600)]) # Extended Pixels
CHARS_SYMOBLS = ''.join([chr(c) for c in range(0x2600, 0x26D0)]) # Symbols
CHARS_ALPHABET = 'abcdefghijklmnopqrstuvwxyz'

# Default characters
DEFAULT_CHARS = ' -\\|/-#[];:_(){}0AVv^<>.,@=+$!~`\'"%*'
# CHARS+='╱╲╳│─╭╮╯╰⇄⇅⇆⇇⇈⇉⇊←↑→↓↔↕↖↗↘↙↚↛↜↝↞↟↰↱↲↳↴↵↶↷↸↹↺↻⇐⇑⇒⇓⇔⇕⇖⇗⇘⇙⇚⇛∀∁∂∃∄∅∆∇∈∉∊∋∌∍∎∏'
# CHARS+='∫∬∭∮∯∰∱∲∳≠≡≢≡≤≥'
# CHARS+='⅐⅑⅒⅓⅔⅕⅖⅗⅘⅙⅚⅛⅜⅝⅞'

default_font = "/usr/share/fonts/TTF/DejaVuSansMono.ttf"
default_font_size = 12


def render_char(c, font):
    image = Image.new('RGB', cell_size, ImageColor.getrgb('#ffffff'))
    draw = ImageDraw.Draw(image)
    draw.text((0, 0), c, font=font, fill=(0, 0, 0))

    # Normalize
    image = np.array(image)[:, :, 0]
    image = (image / 255.) * 2 - 1
    return image


def argmax(x):
    """
    Return index of maximum value.
    """
    idx = (0, 0)
    m = x[idx]

    for row, i in zip(x, range(len(x))):
        j = np.argmax(row)
        locm = row[j]
        if locm > m:
            m = locm
            idx = (i, j)
    return idx


def correlation2(a, b):
    return np.sum(a * b)


def correlation(a, b):
    w = 0.4
    return np.sum((a - w * np.mean(a)) * (b - w * np.mean(b)))


def squareDiff(a, b):
    return -np.sum((a - b) ** 2)


def shift_correl(a, correl):
    cor = fftshift(np.abs(ifft2(fft2(a) * correl)))

    idx = argmax(cor)
    max = cor[idx]
    dist = np.sqrt((idx[0] - cor.shape[0]) ** 2 + (idx[1] - cor.shape[1]) ** 2)

    return max / (10 + dist)


def best_match(correlator_function, cell, correlator_bank):
    correls = [correlator_function(cell, c) for c in correlator_bank]
    max_idx = np.argmax(correls)
    return CHARS[max_idx]


def best_match_v2(cell, correlator_bank_fft):
    correls = [shift_correl(cell, cf) for cf in correlator_bank_fft]
    max_idx = np.argmax(correls)
    return CHARS[max_idx]


def to_skeleton(img, thickness=3):
    thickness = int(thickness + 0.5)
    thresh = threshold_otsu(img)
    img = img > thresh  # convert to binary
    img = 1 - img
    img = skeletonize(img)
    img = 1 - img
    img = erosion(img, square(thickness))
    return img.astype(float)


def normalize(arr):
    """
    Normalize onto range [-1, 1]
    """
    arr = arr - np.min(arr)
    return (arr / np.max(arr)) * 2 - 1


_correlators = {
    "minDiffSquared": squareDiff,
    "maxCorrelation": correlation,
}

if __name__ == '__main__':

    parser = ArgumentParser()

    parser.add_argument("-i", "--input", dest="filename", type=str,
                        required=True,
                        help="Input image.", metavar="IMAGE")

    parser.add_argument("--characters", type=str,
                        dest="characters", default="", metavar="CHARACTERS",
                        help="Use this set of characters to generate the image. Don't forget to mask special characters such as ;.")

    parser.add_argument("--use-pixels", required=False, action="store_true", help="Use pixel characters.")
    parser.add_argument("--use-ext-pixels", required=False, action="store_true", help="Use extended pixel characters.")
    parser.add_argument("--use-symbols", required=False, action="store_true", help="Use unicode symbol characters.")

    parser.add_argument("--invert", required=False, action="store_true", help="Invert the brighness (swap white and black).")

    parser.add_argument("-s", "--scale", metavar="SCALING_FACTOR",
                        dest="scaling_factor", default=1, type=float,
                        help="Scale the image before processing.")

    parser.add_argument("--fuzzyness", metavar="FUZZINESS",
                        dest="fuzziness", default=0.1, type=float,
                        help="Apply gaussian blur to characters before processing.")

    parser.add_argument("-f", "--font", metavar="FONT_FILE",
                        dest="font_file", default=default_font, type=str,
                        help="Use this font to analyze the image.")

    parser.add_argument("-x", "--fontsize", metavar="FONT_SIZE",
                        dest="font_size", default=default_font_size, type=int,
                        help="Use this font to analyze the image.")

    correlators = list(_correlators.keys())
    parser.add_argument("-c", "--correlator", metavar="CORRELATOR",
                        dest="correlator", type=str,
                        default=correlators[0],
                        choices=correlators,
                        help="Choose correlation strategy. One of: [{}].".format(", ".join(correlators)))

    parser.add_argument("--skeletonize",
                        action="store_true", dest="skeletonize", default=False,
                        help="Skeletonize the image (detects edges).")

    args = parser.parse_args()

    CHARS = args.characters

    if args.use_pixels:
        CHARS += CHARS_PIXELS

    if args.use_ext_pixels:
        CHARS += CHARS_EXTENDED_PIXELS

    if args.use_symbols:
        CHARS += CHARS_SYMBOLS

    if not CHARS:
        CHARS = DEFAULT_CHARS

    CHARS = [c for c in CHARS]

    scaling = args.scaling_factor

    fontsize = args.font_size

    font = ImageFont.truetype(args.font_file, fontsize)

    image = Image.new('RGB', (64, 64), ImageColor.getrgb('#ffffff'))
    draw = ImageDraw.Draw(image)
    cell_size = (int(round(fontsize / 1.6)), int(round(fontsize / 0.8)))

    width = cell_size[0]

    # Build the correlator bank
    correlator_bank = np.array([render_char(c, font) for c in CHARS])
    correlator_bank = np.array([gaussian(c, args.fuzziness * width / 2) for c in correlator_bank])
    # correlator_bank_fft = [np.conjugate(fft2(c)) for c in correlator_bank]

    # imshow(correlator_bank[7])
    # show()

    # Read the image
    img = imread(args.filename)
    # Convert to gray scale.
    is_rgba = img.shape[2] == 4
    if is_rgba:
        # Convert transparency to white background
        img = color.rgba2rgb(img, background=[1., 1., 1.]) # Replace transparency by a white background.
    img = color.rgb2gray(img)
    img = rescale(img, scaling)

    imh, imw = img.shape
    cellh, cellw = correlator_bank[0].shape

    # make divisible by cell size
    img = resize(img, (imh - (imh) % cellh + cellh, imw - (imw) % cellw + cellw))

    if args.skeletonize:
        img = to_skeleton(img, fontsize / 2)
    # img = gaussian(img, 1)
    # imshow(img)
    # show()

    if args.invert:
        img = 1-img

    # Normalize onto range [-1, 1]
    img = normalize(img)

    #avg = np.mean(img)
    #img -= avg

    imh, imw = img.shape

    ny = int(imh / cellh)
    nx = int(imw / cellw)

    xgrid, ygrid = np.meshgrid(np.arange(nx), np.arange(ny))

    asciiart = np.zeros(xgrid.shape, dtype=np.chararray)
    asciiart[:, :] = '?'

    correlator_function = correlation
    correlator_function = squareDiff
    correlator_function = _correlators[args.correlator]

    for x, y in zip(xgrid.flatten(), ygrid.flatten()):
        bm = best_match(correlator_function, img[y * cellh:y * cellh + cellh, x * cellw:x * cellw + cellw],
                        correlator_bank)
        # bm = best_match_v2(img[y*cellh:y*cellh+cellh, x*cellw:x*cellw+cellw], correlator_bank_fft)
        asciiart[y, x] = bm

    art = '\n'.join([''.join(a) for a in asciiart])

    art = art + '\n'

    fp = os.fdopen(sys.stdout.fileno(), 'wb')
    fp.write(art.encode())
    fp.flush()
    print('\n')
